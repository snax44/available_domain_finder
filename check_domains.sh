#!/bin/bash

available_file="./available.out"
letters="abcdefghijklmnopqrstuvwxyz"
# Function to display help
function _usage() {
  echo "Usage: $0 --tld TLD --domain-length LENGTH"
  echo
  echo "Options:"
  echo "  --tld             The domain extension (e.g., com, net, org)"
  echo "  --domain-length   The length of the domain name (e.g., 3)"
  echo "  --letters         Letters to use (Default, abcdefghijklmnopqrstuvwxyz)"
}

# Checking the arguments
if [[ $# -lt 4 ]]; then
  _usage
  exit 1
fi

# Parsing arguments
while [[ $# -gt 0 ]]
do
  case $1 in
    --tld)
    tld="$2"
    shift 2 
    ;;
    --domain-length)
    domain_length="$2"
    shift 2 
    ;;
    --letters)
    letters="$2"
    shift 2 
    ;;
    *)
    _usage
    exit 1
    ;;
  esac
done

# Checking mandatory parameters
if [[ -z "${tld}" || -z "${domain_length}" ]]; then
  _usage
  exit 1
fi

# Function to generate combinations of letters
function _generate_combinations() {
  local length=${1}
  local prefix=${2}

  if [[ ${length} -eq 0 ]]; then
    echo "${prefix}"
  else
    for letter in $(echo ${letters} | fold -w1); do
      _generate_combinations $((length - 1)) "${prefix}${letter}"
    done
  fi
}

# Function to check the availability of a domain
function _check_domain() {
  local domain=${1}
  if whois "${domain}" | grep -iq "No match for"; then
    echo "Available: ${domain}"
    echo "${domain}" >> ${available_file}
  else
    echo "Not available: ${domain}"
  fi
}

# Generating combinations and checking domains
for name in $(_generate_combinations ${domain_length} ""); do
  domain="${name}.${tld}"
  _check_domain "${domain}"
done

